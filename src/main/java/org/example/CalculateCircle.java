package org.example;

import java.util.Scanner;

public class CalculateCircle {

    public void calculate () {
        System.out.println("Nhập bán kính của hình tròn ");
        double radius = new Scanner(System.in).nextDouble();
        double chuVi =  (2 * Math.PI * radius);
        double dienTich = Math.PI * radius *radius ;

        System.out.println("Với bán kính = " +radius +
                "\n chu vi = "+ chuVi +
                "\n diện tích bằng = "+ dienTich);
    }
}

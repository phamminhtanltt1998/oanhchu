package org.example;

import java.sql.SQLOutput;
import java.util.Scanner;

public class CheckEvenOrOdd {
    public void checkNumberEvenOrOdd() {

        System.out.println("Nhập số nguyên ");
        int number = new Scanner(System.in).nextInt();
        if (number % 2 == 0) {
            System.out.println("Số " + number + " là số chẵn");
        } else {
            System.out.println("Số " + number + " là số lẻ");
        }
    }
}

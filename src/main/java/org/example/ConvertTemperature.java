package org.example;

import java.util.Scanner;

public class ConvertTemperature {

    public void convertDegree() {

        Scanner sc = new Scanner(System.in);
        System.out.println("Chuyển đổi nhiệt độ");
        System.out.println("Mời nhập nhiệt độ :");
        float temperature = sc.nextFloat();
        System.out.println("Mời nhập kiểu chuyển đổi nhiệt độ :\n" +
                "1. C to F \n" +
                "2. F to C \n"
        );
        int convertType = sc.nextInt();

        if (convertType == ConvertType.C_TO_F.value) {
            float tempConverted = convertCToF(temperature);
            System.out.println(temperature +" độ C = "+ tempConverted +" độ F" );
        } else if (convertType == ConvertType.F_TO_C.value) {
            float tempConverted = convertFToC(temperature);
            System.out.println(temperature +" độ F = "+ tempConverted +" độ C" );
        } else {
            System.out.println("Không hỗ trợ kiểu chuyển đổi");
        }


    }

    public float convertCToF(float temperatureInC) {
        var temperatureInF = (temperatureInC * 9/5) + 32 ;
        return temperatureInF;

    }

    public float convertFToC(float temperatureInF) {
        var temperatureInC = (temperatureInF - 32) * 5/9;
        return temperatureInC;

    }



}

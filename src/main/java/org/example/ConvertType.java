package org.example;

public  enum ConvertType {
    C_TO_F(1),
    F_TO_C(2);

    public  final int value ;
    ConvertType(int value) {
        this.value = value;
    }

}

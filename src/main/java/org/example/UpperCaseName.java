package org.example;

import java.util.Scanner;

public class UpperCaseName {

    public void upperCaseName() {
        System.out.println("Nhập tên ");
        String name = new Scanner(System.in).nextLine();
        String nameUpperCase = name.toUpperCase();
        System.out.println("Tên được uppercase " + nameUpperCase + " độ dài " + nameUpperCase.length());
    }
}

package org.example;

import java.util.Scanner;

public class GiaiThua {
    public void calculate() {
        System.out.println("Nhập vào số cần tính ");
        int number = Integer.parseInt(new Scanner(System.in).nextLine());
        if (number >= 15 ) {
            System.out.println("Số nhập vào nhỏ hơn 15 ");
        }
        int result = 1;
        for (int i = number; i > 0; i--) {
            result *= i ;
        }
        System.out.println("Giai thua của "+ number + "! = "+ result);
        return ;
    }
}
